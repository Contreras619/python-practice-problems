# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    MaxValue = max(values)
    if len(values) == 0:
        return None
    else:
        return MaxValue
