# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    u,l,a,d=0,0,0,0
    if len(password)>=6 and len(password)<=12:
        for i in password:
            if (i.isupper()):
                u=u+1
            if (i.islower()):
                l+=1
            if (i=="@" or i=="!" or i=="$"):
                a+=1
            if (i.isdigit()):
                d+=1
    if (u>=1 and l>=1 and a>=1 and d>=1):
        return ("valid")
    else:
        return ("invalid")
    has
